import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import java.net.*;
import java.util.Scanner;
import java.io.*;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;

public class Authentification implements Runnable {

    private Socket socket;
    private PrintWriter out = null;
    private BufferedReader in = null;
    private String login = "zero", pass =  null;
    public boolean authentifier = false;
    public Thread t2;

    public Authentification(Socket s){
        socket = s;
    }
    public void run() {

        try {

            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream());

            while(!authentifier){

                out.println("Entrez votre login :");
                out.flush();
                login = in.readLine();


                out.println("Entrez votre mot de passe :");
                out.flush();
                pass = in.readLine();

                if(isValid(login, pass)){

                    out.println("connecte");
                    System.out.println(login +" vient de se connecter ");
                    out.flush();
                    authentifier = true;
                }
                else {out.println("erreur"); out.flush();}
            }
            t2 = new Thread(new Chat_ClientServeur(socket,login));
            t2.start();

        } catch (IOException e) {

            System.err.println(login+" ne répond pas !");
        }
    }

    private static boolean isValid(String login, String pass) {

        boolean connexion = false;

        MongoDatabase db = ConnexionDB.connexionToDb();
        MongoCollection collection = db.getCollection("users");

        Object myDoc = collection.find(and(eq("login", login),eq("mdp", pass))).first();

        if(myDoc != null){
            connexion=true;
        }

        return connexion;

    }

}
