/**
 * Created by Nicolas on 16/12/2016.
 */


import com.mongodb.*;
import com.mongodb.client.MongoDatabase;


public class ConnexionDB {

        public static MongoDatabase connexionToDb() {
            try{

                // To connect to mongodb server
                MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
                MongoDatabase db = mongoClient.getDatabase( "test" );
                return db;

            }catch(Exception e){
                System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            }
            return null;
        }
    }

