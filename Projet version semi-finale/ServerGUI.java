/**
 * Created by Nicolas on 01/02/2017.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/*
 * Serveur GUI
 */
public class ServerGUI extends JFrame implements ActionListener, WindowListener {

    private static final long serialVersionUID = 1L;
    // Bouton Stop/Start
    private JButton stopStart;
    // JTextArea pour le chat ou events
    private JTextArea chat, event;
    private JTextField tPortNumber;
    private Server server;


    ServerGUI(int port) {
        super("Chat Server");
        server = null;
        // northPanel : Start / Stop Bouton
        JPanel north = new JPanel();
        north.add(new JLabel("Port : "));
        tPortNumber = new JTextField("  " + port);
        north.add(tPortNumber);
        stopStart = new JButton("Démarrer");
        stopStart.addActionListener(this);
        north.add(stopStart);
        add(north, BorderLayout.NORTH);

        // Chat
        JPanel center = new JPanel(new GridLayout(2,1));
        chat = new JTextArea(80,80);
        chat.setEditable(false);
        appendRoom("Chat.\n");
        center.add(new JScrollPane(chat));
        event = new JTextArea(80,80);
        event.setEditable(false);
        appendEvent("Events.\n");
        center.add(new JScrollPane(event));
        add(center);

        // Information quand utilisateur clique sur le bouton Stop
        addWindowListener(this);
        setSize(400, 600);
        setVisible(true);
    }

    // append message pour les deux JTextArea (Chat et Events)
    void appendRoom(String str) {
        chat.append(str);
        chat.setCaretPosition(chat.getText().length() - 1);
    }
    void appendEvent(String str) {
        event.append(str);
        event.setCaretPosition(chat.getText().length() - 1);

    }

    // Start / Stop bouton
    public void actionPerformed(ActionEvent e) {
        // Si running, stop
        if(server != null) {
            server.stop();
            server = null;
            tPortNumber.setEditable(true);
            stopStart.setText("Démarrer");
            return;
        }


        int port;
        try {
            port = Integer.parseInt(tPortNumber.getText().trim());
        }
        catch(Exception er) {
            appendEvent("Port invalide");
            return;
        }
        // Création du serveur
        server = new Server(port, this);
        // Et démarrage du thread
        new ServerRunning().start();
        stopStart.setText("Arret");
        tPortNumber.setEditable(false);
    }

    // Démarrage du serveur
    public static void main(String[] arg) {
        // port par défaut : 1500
        new ServerGUI(1500);
    }

    /*
     * Si l'utilisateur ferme l'application : arret du serveur
     */
    public void windowClosing(WindowEvent e) {
        if(server != null) {
            try {
                server.stop();
            }
            catch(Exception eClose) {
            }
            server = null;
        }
        dispose();
        System.exit(0);
    }
    public void windowClosed(WindowEvent e) {}
    public void windowOpened(WindowEvent e) {}
    public void windowIconified(WindowEvent e) {}
    public void windowDeiconified(WindowEvent e) {}
    public void windowActivated(WindowEvent e) {}
    public void windowDeactivated(WindowEvent e) {}

    /*
     * Thread run server
     */
    class ServerRunning extends Thread {
        public void run() {
            server.start();
            stopStart.setText("Démarrer");
            tPortNumber.setEditable(true);
            appendEvent("Serveur crashed\n");
            server = null;
        }
    }

}

