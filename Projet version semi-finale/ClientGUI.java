/**
 * Created by Nicolas on 01/02/2017.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


/*
 * Client GUI
 */
public class ClientGUI extends JFrame implements ActionListener {

    private static final long serialVersionUID = 1L;
    private JLabel label;
    private JTextField tf;
    private JTextField tfServer, tfPort;
    private JButton login, logout, listeConnectee;
    private JTextArea ta;
    private boolean connected;
    private Client client;
    private int defaultPort;
    private String defaultHost;

    // Constructeur
    ClientGUI(String host, int port) {

        super("Chat Client");
        defaultPort = port;
        defaultHost = host;

        // NorthPanel :
        JPanel northPanel = new JPanel(new GridLayout(3,1));
        // Serveur et port
        JPanel serverAndPort = new JPanel(new GridLayout(1,5, 1, 3));
        // JTextField du serveur / port avec valeur par défaut
        tfServer = new JTextField(host);
        tfPort = new JTextField("" + port);
        tfPort.setHorizontalAlignment(SwingConstants.RIGHT);

        serverAndPort.add(new JLabel("Adresse serveur : "));
        serverAndPort.add(tfServer);
        serverAndPort.add(new JLabel("Port : "));
        serverAndPort.add(tfPort);
        serverAndPort.add(new JLabel(""));
        northPanel.add(serverAndPort);

        // Label et JtextField
        label = new JLabel("Entrez votre nom d'utilisateur", SwingConstants.CENTER);
        northPanel.add(label);
        tf = new JTextField("Anonymous");
        tf.setBackground(Color.WHITE);
        northPanel.add(tf);
        add(northPanel, BorderLayout.NORTH);

        // CenterPanel (Chat)
        ta = new JTextArea("Hey coucou toi !\n", 80, 80);
        JPanel centerPanel = new JPanel(new GridLayout(1,1));
        centerPanel.add(new JScrollPane(ta));
        ta.setEditable(false);
        add(centerPanel, BorderLayout.CENTER);

        // Les 3 boutons
        login = new JButton("Login");
        login.addActionListener(this);
        logout = new JButton("Logout");
        logout.addActionListener(this);
        logout.setEnabled(false);		// you have to login before being able to logout
        listeConnectee = new JButton("Liste utilisateurs");
        listeConnectee.addActionListener(this);
        listeConnectee.setEnabled(false);		// you have to login before being able to Who is in

        JPanel southPanel = new JPanel();
        southPanel.add(login);
        southPanel.add(logout);
        southPanel.add(listeConnectee);
        add(southPanel, BorderLayout.SOUTH);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(600, 600);
        setVisible(true);
        tf.requestFocus();

    }

    // Appelle par le client pour mettre un message dans le TextArea
    void append(String str) {
        ta.append(str);
        ta.setCaretPosition(ta.getText().length() - 1);
    }
    // Reset bouton si connection failed
    void connectionFailed() {
        login.setEnabled(true);
        logout.setEnabled(false);
        listeConnectee.setEnabled(false);
        label.setText("Entrez votre nom d'utilisateur");
        tf.setText("Anonymous");
        tfPort.setText("" + defaultPort);
        tfServer.setText(defaultHost);
        tfServer.setEditable(false);
        tfPort.setEditable(false);
        tf.removeActionListener(this);
        connected = false;
    }

    /*
    * Event Boutons
    */
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        // Logout : déconnection
        if(o == logout) {
            client.sendMessage(new ChatMessage(ChatMessage.LOGOUT, ""));
            return;
        }
        // Liste des utilisateurs
        if(o == listeConnectee) {
            client.sendMessage(new ChatMessage(ChatMessage.LISTCONNECTEE, ""));
            return;
        }


        //Default Event
        if(connected) {
            // Envoie du message
            client.sendMessage(new ChatMessage(ChatMessage.MESSAGE, tf.getText()));
            tf.setText("");
            return;
        }


        if(o == login) {
            // Demande de connection si OK
            String username = tf.getText().trim();
            // Si utilisateur vide: ignore
            if(username.length() == 0)
                return;
            // Si serveur address vide : ignore
            String server = tfServer.getText().trim();
            if(server.length() == 0)
                return;
            // Si port vide : ignore
            String portNumber = tfPort.getText().trim();
            if(portNumber.length() == 0)
                return;
            int port = 0;
            try {
                port = Integer.parseInt(portNumber);
            }
            catch(Exception en) {
                return;
            }

            // Création du Client
            client = new Client(server, port, username, this);
            // Test si le client démarre
            if(!client.start())
                return;
            tf.setText("");
            label.setText("Entrez votre message");
            connected = true;

            // Virer le bouton Login
            login.setEnabled(false);
            // Activer les 2 autres
            logout.setEnabled(true);
            listeConnectee.setEnabled(true);
            // Virer les JtextField pour la connection
            tfServer.setEditable(false);
            tfPort.setEditable(false);
            // Listener pour les messages de l'utilisateur
            tf.addActionListener(this);
        }

    }


    public static void main(String[] args) {
        new ClientGUI("localhost", 1500);
    }

}
