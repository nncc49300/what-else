/**
 * Created by Nicolas on 01/02/2017.
 */
import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;

/*
 * Le serveur peut etre run en GUI ou en console
 */
public class Server {
    // ID Unique
    private static int uniqueId;
    // Liste des clients
    private ArrayList<ClientThread> al;
    // Variable si GUI
    private ServerGUI sg;
    // Heure
    private SimpleDateFormat sdf;
    // Port
    private int port;
    // Boolean pour éteindre le serveur
    private boolean running;


    /*
     *  server constructor that receive the port to listen to for connection as parameter
     *  in console
     */
    public Server(int port) {
        this(port, null);
    }

    public Server(int port, ServerGUI sg) {
        // GUI ?
        this.sg = sg;
        // le port
        this.port = port;
        // Affichage de l'heure formaté
        sdf = new SimpleDateFormat("HH:mm:ss");
        // Liste des clients
        al = new ArrayList<ClientThread>();
    }

    public void start() {
        running = true;
		/* Création du socket serveur et attente des connections */
        try
        {
            // Socket serveur
            ServerSocket serverSocket = new ServerSocket(port);

            while(running)
            {
                display("Serveur en attente de connextion sur le port: " + port + ".");

                Socket socket = serverSocket.accept();  	// accepter connection
                // Si arret du serveur
                if(!running)
                    break;
                ClientThread t = new ClientThread(socket);  // Thread pour chaque client co
                al.add(t);									// AJouter a la liste des clients
                t.start();
            }
            // Si arret du serveur
            try {
                serverSocket.close();
                for(int i = 0; i < al.size(); ++i) {
                    ClientThread tc = al.get(i);
                    try {
                        tc.sInput.close();
                        tc.sOutput.close();
                        tc.socket.close();
                    }
                    catch(IOException ioE) {

                    }
                }
            }
            catch(Exception e) {
                display("Exception durant la fermeture des client ou du serveur: " + e);
            }
        }
        catch (IOException e) {
            String msg = sdf.format(new Date()) + " Exception création ServerSocket: " + e + "\n";
            display(msg);
        }
    }
    /*
     * Pour arreter le server via GUI
     */
    protected void stop() {
        running = false;
        // Socket socket = serverSocket.accept();
        try {
            new Socket("localhost", port);
        }
        catch(Exception e) {

        }
    }
    /*
     * Afficher un event
     */
    private void display(String msg) {
        String time = sdf.format(new Date()) + " " + msg;
        if(sg == null)
            System.out.println(time);
        else
            sg.appendEvent(time + "\n");
    }
    /*
     *  Broadcast message pour tous les clients
     */
    private synchronized void broadcast(String message) {
        String time = sdf.format(new Date());
        String messageLf = time + " " + message + "\n";
        if(sg == null)
            System.out.print(messageLf);
        else
            sg.appendRoom(messageLf);

        // Reverse loop pour virer un client déconnecté
        for(int i = al.size(); --i >= 0;) {
            ClientThread ct = al.get(i);
            if(!ct.writeMsg(messageLf)) {
                al.remove(i);
                display("Client déconnecté " + ct.username + " viré de la liste.");
            }
        }
    }

    // Bouton déconnecté GUI
    synchronized void remove(int id) {
        // Scan de la liste client pour trouver le bon
        for(int i = 0; i < al.size(); ++i) {
            ClientThread ct = al.get(i);
            // found it
            if(ct.id == id) {
                al.remove(i);
                return;
            }
        }
    }

    /*
     *  Commande pour le server en console :
     * > java Server
     * > java Server Port
     * Port par défaut : 1500
     */
    public static void main(String[] args) {
        int portNumber = 1500;
        switch(args.length) {
            case 1:
                try {
                    portNumber = Integer.parseInt(args[0]);
                }
                catch(Exception e) {
                    System.out.println("Port invalide.");
                    System.out.println("Utilisation : java Server [Port]");
                    return;
                }
            case 0:
                break;
            default:
                System.out.println("Utilisation : java Server [Port]");
                return;

        }
        Server server = new Server(portNumber);
        server.start();
    }

    /** Une instance thread créé pour chaque client */
    class ClientThread extends Thread {
        Socket socket;
        ObjectInputStream sInput;
        ObjectOutputStream sOutput;
        int id;
        String username;
        ChatMessage cm;
        String date;

        // Constructore
        ClientThread(Socket socket) {
            id = ++uniqueId;
            this.socket = socket;
			/* Ouverture des flux d'entré et de sortie */
            System.out.println("Ouverture des flux entrée/sortie");
            try
            {
                // Output en premier (me demande pas pourquoi, c'est google qui l'a dit)
                sOutput = new ObjectOutputStream(socket.getOutputStream());
                sInput  = new ObjectInputStream(socket.getInputStream());
                username = (String) sInput.readObject();
                display(username + " vient de se connecter.");
            }
            catch (IOException e) {
                display("Exception création flux: " + e);
                return;
            }
            catch (ClassNotFoundException e) {
            }
            date = new Date().toString() + "\n";
        }

        public void run() {
            boolean running = true;
            while(running) {
                try {
                    cm = (ChatMessage) sInput.readObject();
                }
                catch (IOException e) {
                    display(username + " Exception lecture flux: " + e);
                    break;
                }
                catch(ClassNotFoundException e2) {
                    break;
                }
                String message = cm.getMessage();

                switch(cm.getType()) {

                    case ChatMessage.MESSAGE:
                        broadcast(username + ": " + message);
                        break;
                    case ChatMessage.LOGOUT:
                        display(username + " déconnecté.");
                        running = false;
                        break;
                    case ChatMessage.LISTCONNECTEE:
                        writeMsg("Liste des utilisateurs connectés à " + sdf.format(new Date()) + "\n");
                        for(int i = 0; i < al.size(); ++i) {
                            ClientThread ct = al.get(i);
                            writeMsg((i+1) + ") " + ct.username + " depuis " + ct.date);
                        }
                        break;
                }
            }
            remove(id);
            close();
        }

        // Si faut tout fermer, on arrete les flux
        private void close() {
            try {
                if(sOutput != null) sOutput.close();
            }
            catch(Exception e) {}
            try {
                if(sInput != null) sInput.close();
            }
            catch(Exception e) {};
            try {
                if(socket != null) socket.close();
            }
            catch (Exception e) {}
        }

        /*
         * Envoie message au client
         */
        private boolean writeMsg(String msg) {
            if(!socket.isConnected()) {
                close();
                return false;
            }
            try {
                sOutput.writeObject(msg);
            }
            catch(IOException e) {
                display("Error envoie message à " + username);
                display(e.toString());
            }
            return true;
        }
    }
}

