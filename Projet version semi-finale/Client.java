/**
 * Created by Nicolas on 01/02/2017.
 */
import java.net.*;
import java.io.*;
import java.util.*;

/*
 * Le clients peut etre démarrer via GUI ou en console
 */
public class Client  {

    private ObjectInputStream sInput;
    private ObjectOutputStream sOutput;
    private Socket socket;

    // GUI ?
    private ClientGUI cg;

    private String server, username;
    private int port;

    /*
     *  Constructeur si console
     */
    Client(String server, int port, String username) {
        this(server, port, username, null);
    }

    /*
     * Constructeur si GUI
     */
    Client(String server, int port, String username, ClientGUI cg) {
        this.server = server;
        this.port = port;
        this.username = username;
        this.cg = cg;
    }

    /*
     * Run dialogue
     */
    public boolean start() {
        try {
            socket = new Socket(server, port);
        }
        catch(Exception ec) {
            display("Erreur connection serveur:" + ec);
            return false;
        }

        String msg = "Connection accepté " + socket.getInetAddress() + ":" + socket.getPort();
        display(msg);

		/* Création flux */
        try
        {
            sInput  = new ObjectInputStream(socket.getInputStream());
            sOutput = new ObjectOutputStream(socket.getOutputStream());
        }
        catch (IOException eIO) {
            display("Exception création flux: " + eIO);
            return false;
        }

        // Création thread
        new ListenFromServer().start();
        // Envoie du username, tous les autres envoi seront considéré comme des messages
        try
        {
            sOutput.writeObject(username);
        }
        catch (IOException eIO) {
            display("Exception connextion utilisateur : " + eIO);
            disconnect();
            return false;
        }
        return true;
    }

    /*
     * Envoie du message sur GUI ou en console
     */
    private void display(String msg) {
            cg.append(msg + "\n");
    }

    /*
     * Envoie d'un message au serveur
     */
    void sendMessage(ChatMessage msg) {
        try {
            sOutput.writeObject(msg);
        }
        catch(IOException e) {
            display("Exception envoie message serveur: " + e);
        }
    }

    /*
     * Tout fermer si quelque chose ne va pas ou si déconnection de l'utilisateur
     */
    private void disconnect() {
        try {
            if(sInput != null) sInput.close();
        }
        catch(Exception e) {}
        try {
            if(sOutput != null) sOutput.close();
        }
        catch(Exception e) {}
        try{
            if(socket != null) socket.close();
        }
        catch(Exception e) {}

        // message to GUI
        if(cg != null)
            cg.connectionFailed();

    }
    /*
     * Commandes pour le mode console :
     * > java Client
     * > java Client username
     * > java Client username portNumber
     * > java Client username portNumber serverAddress
     * Username par défaut : anonyme
     * Port par défaut : 1500
     * Adresse serveur par défaut : localhost
     * > java Client
     * est équivalent à :
     * > java Client Anonymous 1500 localhost
     */
    public static void main(String[] args) {
        int portNumber = 1500;
        String serverAddress = "localhost";
        String userName = "Anonymous";

        // Nombre d'argument de la commande Client
        switch(args.length) {
            case 3:
                serverAddress = args[2];
            case 2:
                try {
                    portNumber = Integer.parseInt(args[1]);
                }
                catch(Exception e) {
                    System.out.println("Port invalide.");
                    System.out.println("Utilisation: > java Client [username] [portNumber] [serverAddress]");
                    return;
                }
            case 1:
                userName = args[0];
            case 0:
                break;
            // Argument invalide
            default:
                System.out.println("Utilisation: > java Client [username] [portNumber] {serverAddress]");
                return;
        }
        // Client objet
        Client client = new Client(serverAddress, portNumber, userName);
        if(!client.start())
            return;
        //Message de l'utilisateur
        Scanner scan = new Scanner(System.in);

        while(true) {
            String msg = scan.nextLine();

            if(msg.equalsIgnoreCase("deconnection")) {
                client.sendMessage(new ChatMessage(ChatMessage.LOGOUT, ""));
                break;
            }
            // message liste utilisateur
            else if(msg.equalsIgnoreCase("listeutilisateur")) {
                client.sendMessage(new ChatMessage(ChatMessage.LISTCONNECTEE, ""));
            }
            else {				// default message
                client.sendMessage(new ChatMessage(ChatMessage.MESSAGE, msg));
            }
        }
        // disconnect
        client.disconnect();
    }

    /*
     * Class pour l'attente de message utilisateur et ajout dans le GUI
     * ou dans la console
     */
    class ListenFromServer extends Thread {

        public void run() {
            while(true) {
                try {
                    String msg = (String) sInput.readObject();
                    if(cg == null) {
                        System.out.println(msg);
                        System.out.print("> ");
                    }
                    else {
                        cg.append(msg);
                    }
                }
                catch(IOException e) {
                    display("Connection au serveur arretée: " + e);
                    if(cg != null)
                        cg.connectionFailed();
                    break;
                }
                catch(ClassNotFoundException e2) {
                }
            }
        }
    }
}
